import React,  {Component} from 'react';
import './styles.css';
import Music from "../../components/Admin/Music";
 import Musicservice from "../../services/music.service";
 import DocumentService from "../../services/document.service";
 import HeaderAdmin from "../../components/HeaderAdmin";

 import {Link} from 'react-router-dom';

export default class MusicCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      document:null,
      duration:null,
      age_restriction:null,
      musics:[],
      documents:[]
    }
  }
 async componentDidMount(){
     try{
       let responseM = await Musicservice.list();
       let responseD = await DocumentService.list();
       this.setState({musics: responseM.data.music,documents: responseD.data.Documents});
       console.log(responseM.data.music);
     }catch (e){
       console.error(e);
     }
  }

  async deleteDocument(id){
    try{
        await Musicservice.delete(id);
    }catch (e) {
        console.error(e);
    }
}
handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}


async submit(e){
  e.preventDefault();
  let {document,duration,age_restriction} = this.state;
  let body = {document,duration,age_restriction};
  // let formData = new FormData();
  // formData.append('document', document);
  // formData.append('duration',duration);
  // formData.append('age_restriction',age_restriction);
  try{
    console.log(age_restriction);
      await Musicservice.create(body);
      this.props.history.push('/admin/musics');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {musics,documents} = this.state;
    return <div>
       <HeaderAdmin/>
    <div class="add docs">
    <form onSubmit={(e) => this.submit(e)}>
    <div class="title">Add Musics</div>  
    <div class="row">
  
        <div class="col-auto">
          <label  class="form-label">document</label>
          <select class="form-select" id="document" required onChange={(e) => this.handleChange(e)}>
          <option>Document</option>
          {
        documents.map((document, index) => {
       return <option value={document._id}>{document.title}</option>
        })
      }
        </select>
        </div>
        <div class="col-auto">
        <label  class="form-label">duration</label>
        <input type="textarea" id="duration"  col="3" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">age_restriction</label>
        <input type="number" id="age_restriction" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <button className="btn btn-primary" type="submit">Add</button>
        </div>
      </div>

     </form>
    </div>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active">index</th>
        <th class="table-active"> document</th>
        <th class="table-active"> duration</th>
        <th class="table-active"> age_restriction</th>
        <th class="table-active"> update</th>
        <th class="table-active"> delete</th>
        </tr>
        </thead>
      <tbody>
      {
        musics.map((music, index) => {
        return    <tr>
         
        <td>{index}</td>
        
        <td>{ documents.filter(document => document==music.document)}</td> 
  {/* à traiter */}
        <td> {music.duration}</td>
        <td>{music.age_restriction}</td> {/* à traiter */}
        <td><Link to={`/admin/documents/${music.document}`}><button className="btn btn-success" type="submit">update</button></Link></td>
 
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(music._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
      </tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
