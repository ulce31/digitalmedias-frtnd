import axios from 'axios';
export default class documentservice{

 /**
     * Get list of documents
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/documents`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Get detail one document
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async detail(id){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/documents/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Delete one document
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/documents/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }

    /**
     * Create one document
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/documents`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Update one document
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id,body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/documents/${id}`,body, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
}