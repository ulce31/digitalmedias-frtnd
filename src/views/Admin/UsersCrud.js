import React,  {Component} from 'react';
import './styles.css';

 import UserService from "../../services/user.service";
 import HeaderAdmin from "../../components/HeaderAdmin";

 import {Link} from 'react-router-dom';

export default class genreCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      firstname:null,
      lastname:null,
      email:null,
      password:null,
      picture:null,
      country:null,
      city:null,
      postal:null,
      phone:null,
      role:null,
      users:[]
    
    }
  }
 async componentDidMount(){
     try{
       let response = await UserService.list();
       this.setState({users: response.data.users});
       console.log(response.data.users);
     }catch (e){
       console.error(e);
     }
  }

  async deleteDocument(id){
    try{
        await UserService.delete(id);
    }catch (e) {
        console.error(e);
    }
}
handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}
handleChangePicture(e){
  this.setState({
      picture: e.target.files[0]
  });
}

async submit(e){
  e.preventDefault();
  let {firstname,lastname,email,password,picture,country,city,postal,phone,role}= this.state ;

  let formData = new FormData();
  formData.append('firstname', firstname);  
  formData.append('lastname',lastname);
  formData.append('email',email);
  formData.append('password',password);
  formData.append('picture',picture);
  formData.append('country',country);
  formData.append('city',city);
  formData.append('postal',postal);
  formData.append('phone',phone);
  formData.append('role',role);
  
  
 
  try{
    
      await UserService.create(formData);
      this.props.history.push('/users');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {users} = this.state;
    return <div>
       <HeaderAdmin/>
    <div class="add docs">
    <form onSubmit={(e) => this.submit(e)}>
    <div class="title">Add genres</div>  

      <div class="row">
      <div class="col-auto">
        <label  class="form-label">firstname</label>
        <input type="text" id="firstname"   class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      <div class="col-auto">
        <label  class="form-label">lastname</label>
        <input type="text" id="lastname" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        
      <div class="col-auto">
        <button className="btn btn-primary" type="submit">Add</button>
        </div>
      </div>
      <div class="row">
      <div class="col-auto">
        <label  class="form-label">email</label>
        <input type="text" id="email"   class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      <div class="col-auto">
        <label  class="form-label">password</label>
        <input type="text" id="password" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">picture</label>
        <input type="file" id="picture" accept="image/jpeg, image/png"
                            className="form-control"
                            onChange={(e) => this.handleChangePicture(e)}/> </div>
      </div>
      <div class="row">
      <div class="col-auto">
        <label  class="form-label">country</label>
        <input type="text" id="country"   class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      <div class="col-auto">
        <label  class="form-label">city</label>
        <input type="text" id="city" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">postal</label>
        <input type="text" id="postal" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">phone</label>
        <input type="text" id="phone" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      </div>
 
     </form>
    </div>
      <table class="table table-dark">
        <thead>
          <tr>
        <th class="table-active">index</th>
        <th class="table-active">picture</th>
        <th class="table-active"> firstname</th>
        <th class="table-active"> lastname</th>
        <th class="table-active"> email</th>
        <th class="table-active"> country</th>
        <th class="table-active"> city</th>
        <th class="table-active"> postal</th>
        <th class="table-active"> phone</th>
        </tr>
        </thead>
      <tbody>
      {
        users.map((user, index) => {
        return    <tr>
         
        <td>{index}</td>
  {/* à traiter */}
  <td>{
          user.picture && <img  src={user.picture} alt="Card image cap"/>
           }</td>
        <td>{user.firstname}</td>
        <td>{user.lastname}</td>
        <td>{user.email}</td>
        <td>{user.country}</td>
        <td>{user.city}</td>
        <td>{user.postal}</td>
        <td>{user.phone}</td>
        <td><Link to={'/admin/genres'}><button className="btn btn-success" type="submit">update</button></Link></td>
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(user._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
      </tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
