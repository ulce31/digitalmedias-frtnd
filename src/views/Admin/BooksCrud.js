import React,  {Component} from 'react';
import './styles.css';
import Book from "../../components/Admin/Book";
 import BookService from "../../services/book.service";
 import DocumentService from "../../services/document.service";
 import HeaderAdmin from "../../components/HeaderAdmin";

 import {Link} from 'react-router-dom';

export default class BookCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      document:null,
      nbPage:null,
      description:null,
      age:null,
      books:[],
      documents:[]
    }
  }
 async componentDidMount(){
     try{
       let responseB = await BookService.list();
       let responseD = await DocumentService.list();
       this.setState({books: responseB.data.book,documents: responseD.data.Documents});
       console.log(responseB.data.book);
     }catch (e){
       console.error(e);
     }
  }


handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}
async deleteDocument(id){
  try{
      await BookService.delete(id);
  }catch (e) {
      console.error(e);
  }
}

async submit(e){
  e.preventDefault();
 let {document,nbPage,description,age} = this.state;
 let body = {document,nbPage,description,age} ;
  // let formData = new FormData();
  // formData.append('document', document);
  // formData.append('nbPage',nbPage);
  // formData.append('description',description);
  // formData.append('age',age);
  try{
    console.log(age);
      await BookService.create(body);
      this.props.history.push('/admin/Books');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {books,documents} = this.state;
    return <div>
       <HeaderAdmin/>
    <div class="add docs">
    <form onSubmit={(e) => this.submit(e)}>
    <div class="title">Add Books</div>  
    <div class="row">
  
        <div class="col-auto">
          <label  class="form-label">document</label>
          <select class="form-select" id="document"  required onChange={(e) => this.handleChange(e)}>
          <option>Document</option>
          {
        documents.map((document, index) => {
       return <option value={document._id}>{document.title}</option>
        })
      }
        </select>
        </div>
        <div class="col-auto">
        <label  class="form-label">description</label>
        <input type="textarea" id="author" col="3" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">nbPage</label>
        <input type="number" id="nbPage" class="form-control"  onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">Age</label>
        <input type="number" id="age" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      </div>
      <div class="row">
        <div class="col-auto">
        <button className="btn btn-primary" type="submit">Add</button>
        </div>
      </div>
     </form>
    </div>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active">index</th>
        <th class="table-active"> document</th>
        <th class="table-active"> description</th>
        <th class="table-active"> nbPage</th>
        <th class="table-active"> age</th>
        <th class="table-active"> update</th>
        <th class="table-active"> delete</th>
        </tr>
        </thead>
      <tbody>
      {
        books.map((book, index) => {
        return    <tr>
         
        <td>{index}</td>
        
        <td>{ documents.filter(document => document==book.document)}</td> 
  {/* à traiter */}
        <td> {book.description}</td>
        <td>{book.nbPage}</td> {/* à traiter */}
        <td>{book.age}</td>
        <td><Link to={`/admin/documents/${book.document}`}><button className="btn btn-success" type="submit">update</button></Link></td>
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(book._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
      </tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
