import React,  {Component} from 'react';
import './styles.css';
import Document from "../../components/Admin/Document";
 import DocumentService from "../../services/document.service";
 import HeaderAdmin from "../../components/HeaderAdmin";
 import {Link} from 'react-router-dom';
 import moment from 'moment';

export default class DocumentCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      document:[]
    }
  }
 async componentDidMount(){
     try{
      let id = this.props.match.params.id;
       let response = await DocumentService.detail(id);
       this.setState({document: response.data.document});
       console.log(response.data.document.picture);
     }catch (e){
       console.error(e);
     }
  }



  render(){
    let {document} = this.state;
    return <div>
       <HeaderAdmin/>
      <h1>Details Document</h1>
  
 <div className="detail">
   
   <div className="info">
     <div className="rowInfo">
     <span className="titleInfo">Title:</span>  {document.title}
     </div>
     <div className="rowInfo">
     <span className="titleInfo">Date:</span>   {moment(document.created_at).format('D/M/Y')}
     </div>
     <div className="rowInfo">
     <span className="titleInfo">Type:</span> {document.type}
     </div>
     <div className="rowInfo">
     <span className="titleInfo">Author:</span> {document.Author}
     </div>

   </div>
  <div className="image">
    {
          document.picture && <img  src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Question_book-4.svg/771px-Question_book-4.svg.png" className="detailImage" alt="Card image cap"/>
           }</div>
          

   
   </div> 
    </div>
  }

};
