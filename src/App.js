
import React,  {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import axios from 'axios';
import BooksCrud from "./views/Admin/BooksCrud";
import BooksDetails from "./views/Admin/BooksDetails";
import BorrowsCrud from "./views/Admin/BorrowsCrud";
import CommentsCrud from "./views/Admin/CommentsCrud";
import DocumentsCrud from "./views/Admin/DocumentsCrud";
import DocumentsDetails from "./views/Admin/DocumentsDetails";
import GenresCrud from "./views/Admin/GenresCrud";
import MoviesCrud from "./views/Admin/MoviesCrud";
import MusicsCrud from "./views/Admin/MusicsCrud";
import UsersCrud from "./views/Admin/UsersCrud";
import HomeAdmin from "./views/Admin/HomeAdmin";
import DocumentsList from "./views/Documents/DocumentsList";
import Login from "./views/Auth/Login";
import Header from "./components/Header";
import DocumentsList from "./views/Documents/DocumentsList";

export default class App extends Component{
 
  render(){
    return <BrowserRouter>
  
    <h1></h1>
    {/* <Route exact path="/" component={Login} />
    <Route exact path="/register" component={Register} /> */}
    {/* <Route exact path="/admin/documents" component={DocumentsList}/> */}
       {/* Book routes */}
    <Route exact path="/admin" component={HomeAdmin}/>
    <Route exact path="/admin/books" component={BooksCrud}/>
    <Route exact path="/admin/books/:id" component={BooksDetails}/>
    <Route exact path="/admin/borrows" component={BorrowsCrud}/>
    <Route exact path="/admin/comments" component={CommentsCrud}/>
    {/* Document routes */}
    <Route exact path="/admin/documents" component={DocumentsCrud}/>
    <Route exact path="/admin/documents/:id" component={DocumentsDetails}/>
    <Route exact path="/admin/genres" component={GenresCrud}/>
    <Route exact path="/admin/movies" component={MoviesCrud}/>
    <Route exact path="/admin/musics" component={MusicsCrud}/>
    <Route exact path="/admin/users" component={UsersCrud}/>
    <Route exact path="/login" component={Login}/>
    <Route exact path="/home" component={DocumentsList}/>
</BrowserRouter>
  }

};

