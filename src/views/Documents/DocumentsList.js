//import logo from './logo.svg';

import React,  {Component} from 'react';
import Document from "../../components/Document";
import DocumentService from "../../services/document.service";

export default class DocumentsList extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      documents:[]
    }
  }
 async componentDidMount(){
     try{
       let response = await DocumentService.list();
       this.setState({documents: response.data.Documents});
       console.log(response.data.Documents);
     }catch (e){
       console.error(e);
     }
  }
  render(){
    let {documents} = this.state;
    return <div>
      {
        documents.map((document, index) => {
        return  <div className="col-md-4 mb-3">
        <Document
          title={document.title}
          date={document.date}
          genres={document.genre}
          picture={document.picture}
        />
    </div>

        })
      }
    </div>
  }

};
