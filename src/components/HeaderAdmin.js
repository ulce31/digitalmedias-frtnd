import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class HeaderAdmin extends Component{

    render() {
        return <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
             <Link to={'/admin'} className="navbar-brand">DIGITALMEDIAS</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <Link to={'/admin'} className="nav-link">Home</Link>
                    <Link to={'/admin/documents'} className="nav-link">Document</Link>
                    <Link to={'/admin/musics'} className="nav-link">Music</Link>
                    <Link to={'/admin/books'} className="nav-link">Books</Link>
                    <Link to={'/admin/movies'} className="nav-link">Movies</Link>
                    <Link to={'/admin/genres'} className="nav-link">Genres</Link>
                    <Link to={'/admin/borrows'} className="nav-link">Borrows</Link>
                    <Link to={'/admin/users'} className="nav-link">Users</Link>
                    <Link to={'/admin/comments'} className="nav-link">Comments</Link>
                </div>
            </div>
        </nav>
        
    }
}
