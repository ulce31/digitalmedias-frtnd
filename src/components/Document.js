import React,  {Component} from 'react';

export default class Document extends Component{
    render (){
        let {title,genre,date,picture} = this.props;
        return <div className="card">
            
      {
                picture && <img className="card-img-top" src={picture} alt="Card image cap"/>
        
     }

        <div className="card-body">
            <h5 className="card-title">{title}</h5>
    <p className="card-text">{date} {genre}</p>
       
            <a href="#" className="btn btn-primary">Voir le film</a>
        </div>
    </div>

    }
}