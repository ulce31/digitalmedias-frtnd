import React, { Component } from "react";

import UserService from "../../services/user.service";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lastname: null,
      firstname: null,
      city: "N/R",
      postal: "N/R",
      phone: "N/R",
      email: null,
      password: null,
      isError: false,
    };
  }

  handlechange = (e) => {
    console.log(e)
    this.setState({ [e.target.id]: e.target.value });
  };

  async submit(e) {
    console.log();
    e.preventDefault();
    this.setState({ isError: false });
    let { email, password, lastname, firstname, city, postal, phone} = this.state;
    let body = { email, password, lastname, firstname, city, postal, phone };

    try {
      let response = await UserService.add(body);
      console.log(response.data);
      localStorage.setItem("usrToken", response.data.token);
      this.props.updateUser(response.data.user);
    } catch (error) {}
  }

  render() {
    let { isError } = this.state;
    return (
      <div className="container">
        <h1>Videos Add</h1>
        <div className="m-auto">
          <form>
            <div className="row">
              <div className="col-md-6">
              <div className="form-group">
                  <label>Lastname</label>
                  <input
                    type="text"
                    id="lastname"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Firstname</label>
                  <input
                    type="text"
                    id="firstname"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>City (optional)</label>
                  <input
                    type="text"
                    id="city"
                    className="form-control"
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Post code (optional)</label>
                  <input
                    type="text"
                    id="postal"
                    className="form-control"
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Phone (optional)</label>
                  
                  <input
                  id="phone"
                  placeholder="Enter phone number"
                  onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Email</label>
                  <input
                    type="text"
                    id="email"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input
                    type="password"
                    id="password"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-success"
                  onClick={(e) => {
                    this.submit(e);
                  }}
                >
                  Register
                </button>
                {isError && <p>Erreur d'email et / ou de mot de passe</p>}
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
