import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import UserService from "../../services/user.service";

export default class Login extends Component {
  constructor(props) {

    super(props);
    this.state = {
      email: null,
      password: null,
      isError: false,
      navigate: false
    };
  }

  handlechange = (e) => {
    console.log("ID", e.target.id);
    console.log("value", e.target.value);
    this.setState({ [e.target.id]: e.target.value });
  };

  async submit(e) {
    e.preventDefault();
    this.setState({ isError: false });
    let { email, password } = this.state;
    let body = { email, password };

    try {
      let response = await UserService.auth(body);
      console.log(response.data);
      localStorage.setItem("usrToken", response.data.token);

      this.props.updateUser(response.data.user);
    } catch (error) {}
  }

  render() {
    const { navigate } = this.state
    let { isError } = this.state;

    if (navigate) {
      return <Redirect to="/register" push={true} />
    }

    return (
      <div className="container">
        <h1>Videos Add</h1>
        <div className="m-auto">
          <form>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Email</label>
                  <input
                    type="text"
                    id="email"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input
                    type="password"
                    id="password"
                    className="form-control"
                    required
                    onChange={(e) => this.handlechange(e)}
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-success"
                  onClick={(e) => {
                    this.submit(e);
                  }}
                >
                  Connexion
                </button>

                {isError && <p>Erreur d'email et / ou de mot de passe</p>}
              </div>
            </div>
          </form>
          <button
                  className="btn btn-success"
                  onClick={() => this.setState({ navigate: true })}
                >
                  Créer un compte
                </button>
        </div>
      </div>
    );
  }
}
