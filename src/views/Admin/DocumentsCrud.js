import React,  {Component} from 'react';
import './styles.css';
import Document from "../../components/Admin/Document";
 import DocumentService from "../../services/document.service";
 import GenreService from "../../services/genre.service";
 import HeaderAdmin from "../../components/HeaderAdmin";
 import moment from 'moment';
 import {Link} from 'react-router-dom';

export default class DocumentCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      title:null,
      genre:null,
      author:null,
      date:null,
      type:null,
      credits:null,
      picture:null,
      documents:[],
      genres:[]
    }
  }
 async componentDidMount(){
     try{
       let responseD = await DocumentService.list();
       let responseG = await GenreService.list();
       this.setState({documents: responseD.data.Documents,genres: responseG.data.genre});
       console.log(responseG.data.genre);
     }catch (e){
       console.error(e);
     }
  }

  async deleteDocument(id){
    try{
        await DocumentService.delete(id);
    }catch (e) {
        console.error(e);
    }
}

handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}
handleChangePicture(e){
  this.setState({
      picture: e.target.files[0]
  });
}

async submit(e){
  e.preventDefault();
  let {title, genre, author, date, type, credits, picture} = this.state;
  let body = {title, genre, author, date, type, credits, picture};

  // let formData = new FormData();
  // formData.append('title', title);
  // formData.append('genre', genre);
  // formData.append('picture', picture);
  // formData.append('author', author);
  // formData.append('date', date);
  // formData.append('type', type);
  // formData.append('credits', credits);

  try{
    console.log(type);
      await DocumentService.create(body);
      this.props.history.push('/admin/documents');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {documents,genres} = this.state;
    return <div>
       <HeaderAdmin/>
    <div class="add docs">
    <form onSubmit={(e) => this.submit(e)}>
    <div class="title">Add Documents</div>  
    <div class="row">
        <div class="col-auto">
        <label  class="form-label">Title</label>
        <input type="text" id="title" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
          <label  class="form-label">genre</label>
          <select class="form-select" id="genre"  onChange={(e) => this.handleChange(e)}>
          <option>Genre</option>
          {
        genres.map((genre, index) => {

       return <option value={genre._id}>{genre.title}</option>
        })
      }
        </select>
        </div>
        <div class="col-auto">
        <label  class="form-label">author</label>
        <input type="text" id="author" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">Date</label>
        <input type="date" id="date" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      </div>
      <div class="row">
        <div class="col-auto">
        <label  class="form-label">Type</label>
        <select class="form-select" id="type" required onChange={(e) => this.handleChange(e)}>
          <option>Type</option>
          <option value="Book">Book</option>
          <option value="Music">Music</option>
          <option value="Movie">Movie</option>
          </select>
        </div>
        <div class="col-auto">
        <label  class="form-label">credits</label>
        <input type="text" id="credits" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
        <div class="col-auto">
        <label  class="form-label">Picture</label>
        <input type="file" id="picture" accept="image/jpeg, image/png"
                            className="form-control"
                            onChange={(e) => this.handleChangePicture(e)}/>
        </div>
        <div class="col-auto">
        <button className="btn btn-primary" type="submit">Add</button>
        </div>
      </div>
     </form>
    </div>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active">index</th>
        <th class="table-active"> picture</th>
        <th class="table-active"> title</th>
        <th class="table-active"> genre</th>
        <th class="table-active"> author</th>
        <th class="table-active"> date</th>
        <th class="table-active"> type</th>
        <th class="table-active"> credits</th>
        <th class="table-active"> update</th>
        <th class="table-active"> delete</th>
        </tr>
        </thead>
      <tbody>
        

      {
        documents.map((document, index) => {
        return    <tr>
         
        <td>{index}</td>
        <td>{
          document.picture && <img  src={document.picture} alt="Card image cap"/>
           }</td>
        <td>{document.title}</td>
        <td>{genres.filter(genre => genre==document.genre).title}</td> 

  {/* à traiter */}
        <td> {document.author}</td>
        <td>{moment(document.created_at).format('D/M/Y')}</td> {/* à traiter */}
        <td>{document.type}</td>
        <td>{document.credits}</td>
        <td><Link to={`/admin/documents/${document._id}`}><button className="btn btn-success" type="submit">update</button></Link></td>
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(document._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
</tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
