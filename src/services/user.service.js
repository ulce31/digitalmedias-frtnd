import axios from "axios";

export default class UserService {
   /**
     * Get list of documents
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
      return await axios.get(`${process.env.REACT_APP_HOST_API}/users`, {
          headers: {
              'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
          }
      });
  }
  static async auth(body) {
    return await axios.post(`${process.env.REACT_APP_API}/users/auth`, body);
  }

  static async add(body) {
    return await axios.post(`${process.env.REACT_APP_API}/users`, body);
  }
}
