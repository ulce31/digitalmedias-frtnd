import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Header extends Component{

    render() {
        return <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
             <Link to={'/'} className="navbar-brand">DIGITALMEDIAS</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <Link to={'/home'} className="nav-link">Home</Link>
                    <Link to={'/music'} className="nav-link">Music</Link>
                    <Link to={'/movies'} className="nav-link">Movies</Link>
                    <Link to={'/books'} className="nav-link">Books</Link>
                    <Link to={'/categories'} className="nav-link">Catégories</Link>
                    <Link to={'/home'} className="nav-link ml-4">Logout</Link>
                </div>
            </div>
        </nav>
        
    }
}
