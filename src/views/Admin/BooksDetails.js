import React,  {Component} from 'react';
import './styles.css';
import Book from "../../components/Admin/Book";
 import Bookservice from "../../services/book.service";
 import HeaderAdmin from "../../components/HeaderAdmin";
 import {Link} from 'react-router-dom';

export default class BookCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      Books:[]
    }
  }
 async componentDidMount(){
     try{
       let response = await Bookservice.list();
       this.setState({Books: response.data.Books});
       console.log(response.data.Books);
     }catch (e){
       console.error(e);
     }
  }


handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}

async submit(e){
    e.preventDefault();

    try{
        let id = this.props.title;
        let body = {title:this.props.title};
        await Bookservice.update(id, body);
        this.props.history.push('/admin/Books');
    }catch (e) {
        console.error(e);
    }
}
  render(){
    let {Books} = this.state;
    return <div>
       <HeaderAdmin/>
      <h1>Booksmmm</h1>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active"> index</th>
        <th class="table-active"> title</th>
        <th class="table-active"> picture</th>
        <th class="table-active"> genre</th>
        <th class="table-active"> author</th>
        <th class="table-active"> date</th>
        <th class="table-active"> type</th>
        <th class="table-active"> credits</th>
        <th class="table-active"> update</th>
        <th class="table-active"> delete</th>
        </tr>
        </thead>
      <tbody>
        


      </tbody>
    </table>
    </div>
  }

};
