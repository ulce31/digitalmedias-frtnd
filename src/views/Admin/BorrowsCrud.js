import React,  {Component} from 'react';
import './styles.css';
import Movie from "../../components/Admin/Movie";
 import BorrowService from "../../services/borrow.service";
 import DocumentService from "../../services/document.service";
 import UserService from "../../services/user.service";
 import HeaderAdmin from "../../components/HeaderAdmin";

 import {Link} from 'react-router-dom';

export default class BorrowCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      user:null,
      document:null,
      users:[],
      documents:[],
      borrows:[]
    }
  }
 async componentDidMount(){
     try{
       let responseB = await BorrowService.list();
       let responseU = await UserService.list();
       let responseD = await DocumentService.list();
       this.setState({borrows: responseB.data.borrows,users: responseU.data.users,documents: responseD.data.Documents});
       console.log(responseB.data.borrows);
     }catch (e){
       console.error(e);
     }
  }


handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}
async deleteDocument(id){
  try{
      await BorrowService.delete(id);
  }catch (e) {
      console.error(e);
  }
}

async submit(e){
  e.preventDefault();
  let {user,document}= this.state ;
  let body =  {user,document};
  // let formData = new FormData();
  // formData.append('user', user);
  // formData.append('document',document);
  try{
    console.log(user);
      await BorrowService.create(body);
      this.props.history.push('/borrows');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {documents,users,borrows} = this.state;
    return <div>
       <HeaderAdmin/>
       <div class="title plus">List of borrows</div>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active">index</th>
        <th class="table-active"> user</th>
        <th class="table-active"> document</th>
        <th class="table-active"> date</th>
        </tr>
        </thead>
      <tbody>
      {
        borrows.map((borrow, index) => {
        return    <tr>
         
        <td>{index}</td>
        
        <td>{ users.filter(user => user==borrow.user)}</td> 
  {/* à traiter */}
  <td>{ documents.filter(document => document==borrow.user)}</td> 
        <td><Link to={'/admin/movies'}><button className="btn btn-success" type="submit">update</button></Link></td>
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(borrow._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
      </tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
