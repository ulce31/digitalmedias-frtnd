import React,  {Component} from 'react';
import './styles.css';

 import GenreService from "../../services/genre.service";
 import HeaderAdmin from "../../components/HeaderAdmin";

 import {Link} from 'react-router-dom';

export default class genreCrud extends Component{
  constructor(props){
    console.log("constructor");
    super(props);
    this.state= {
      title:null,
      description:null,
      genres:[]
    
    }
  }
 async componentDidMount(){
     try{
       let response = await GenreService.list();
       this.setState({genres: response.data.genre});
       console.log(response.data.genre);
     }catch (e){
       console.error(e);
     }
  }
  async deleteDocument(id){
    try{
        await GenreService.delete(id);
    }catch (e) {
        console.error(e);
    }
}

handleChange(e){
    this.setState({
        [e.target.id]: e.target.value
    })
}


async submit(e){
  e.preventDefault();
  let {title, description}= this.state ;
let body = {title, description};
  // let formData = new FormData();
  // formData.append('title', title);  
  // formData.append('description',description);
 
  try{
    console.log(title);
      await GenreService.create(body);
      this.props.history.push('/admin/genres');
  }catch (e) {
      console.error(e);
  }
}


  render(){
    let {genres,documents} = this.state;
    return <div>
       <HeaderAdmin/>
    <div class="add docs">
    <form onSubmit={(e) => this.submit(e)}>
    <div class="title">Add genres</div>  

      <div class="row">
      <div class="col-auto">
        <label  class="form-label">title</label>
        <input type="text" id="title" required  class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      <div class="col-auto">
        <label  class="form-label">description</label>
        <input type="textarea" id="description"  col="3" class="form-control" onChange={(e) => this.handleChange(e)}></input>
        </div>
      <div class="col-auto">
        <button className="btn btn-primary" type="submit">Add</button>
        </div>
      </div>
     </form>
    </div>
      <table class="table table-dark">
        <thead>
          <tr>
          <th class="table-active">index</th>
          <th class="table-active"> title</th>
        <th class="table-active"> description</th>
        <th class="table-active"> update</th>
        <th class="table-active"> delete</th>
        </tr>
        </thead>
      <tbody>
      {
        genres.map((genre, index) => {
        return    <tr>
         
        <td>{index}</td>
  {/* à traiter */}
    
        <td>{genre.title}</td>
        <td>{genre.description}</td>
        <td><Link to={'/admin/genres'}><button className="btn btn-success" type="submit">update</button></Link></td>
        <td><button className="btn btn-danger" onClick={() => this.deleteDocument(genre._id)}> <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                             fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
</button></td>
      </tr>
        })
      }
      </tbody>
    </table>
    </div>
  }

};
