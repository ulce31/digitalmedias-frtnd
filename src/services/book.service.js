import axios from 'axios';
export default class bookService{

 /**
     * Get list of books
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/books`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Get detail one book
     * 
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async detail(id){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/books/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Delete one book
     * 
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/books/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }

    /**
     * Create one book
     * 
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/books`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
    /**
     * Update one book
     * 
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id,body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/books/${id}`,body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenDegital')}`
            }
        });
    }
}